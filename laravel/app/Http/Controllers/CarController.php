<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Car;

class CarController extends Controller
{
    public function index()
    {
        $cars = Car::getAllCars();
    	return view('ravi.cars', compact('cars'));
    }

    public function show($id)
    {
    	$car = Car::getOneCar($id);
    	return view('ravi.car_details', compact('car'));
    }
}
